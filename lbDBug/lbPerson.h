//
//  lbPerson.h
//  lbDBug
//
//  Created by 刘波 on 2022/11/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface lbPerson : NSObject

@property (strong,nonatomic) NSString *name;

@end

NS_ASSUME_NONNULL_END
