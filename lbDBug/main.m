//
//  main.m
//  lbDBug
//
//  Created by 刘波 on 2022/11/28.
//

#import <Foundation/Foundation.h>
#import "lbPerson.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        lbPerson *lb = [[lbPerson alloc] init];
        lb.name = @"lb";
        NSLog(@"姓名：%@",lb.name);
    }
    return 0;
}
